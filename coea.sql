/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : coea

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-09-16 17:26:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for applications
-- ----------------------------
DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `personal_info` longtext COLLATE utf8mb4_unicode_ci,
  `additional_info` longtext COLLATE utf8mb4_unicode_ci,
  `university_education` longtext COLLATE utf8mb4_unicode_ci,
  `highschool_info` longtext COLLATE utf8mb4_unicode_ci,
  `lang_prof` longtext COLLATE utf8mb4_unicode_ci,
  `applicant_desc` longtext COLLATE utf8mb4_unicode_ci,
  `documents` longtext COLLATE utf8mb4_unicode_ci,
  `current_appointment` longtext COLLATE utf8mb4_unicode_ci,
  `references` longtext COLLATE utf8mb4_unicode_ci,
  `scholarships` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of applications
-- ----------------------------
INSERT INTO `applications` VALUES ('14', '3', '[\"Family Name:abualainin\",\"First Name: Mohammed\",\"Middle Name: Adel\",\"Date of Birth:09-10-1993\",\"Place of Birth:Egypt\",\"Gender:Male\"]', '[\"House:31\",\"Street Address:grp 36 bldg 3 apartement 31\",\"City:Rehab\",\"Postal Code:123456\",\"Phone:01201508867\",\"National ID \\/ Passport:29310090101496\"]', '[\"University:AASTMT,College:CE,Degree:BSc\",\"University:Cairo,College:CS,Degree:Masters\"]', '[]', '[]', '[]', '[]', '[]', '[]', '[]', '2019-09-16 14:56:45', '2019-09-16 14:56:45', null, 'New', 'null');
INSERT INTO `applications` VALUES ('15', '3', '[\"Family Name:abualainin\",\"First Name: Mohammed\",\"Middle Name: Adel\",\"Date of Birth:09-10-1993\",\"Place of Birth:Egypt\",\"Gender:Male\"]', '[\"House:31\",\"Street Address:grp 36 bldg 3 apartement 31\",\"City:Rehab\",\"Postal Code:123456\",\"Phone:01201508867\",\"National ID \\/ Passport:29310090101496\"]', null, null, null, null, null, null, null, null, '2019-09-16 15:09:13', '2019-09-16 15:09:13', null, 'New', 'null');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Agriculture', '2019-09-15 13:00:22', '2019-09-15 13:00:22', null);
INSERT INTO `categories` VALUES ('2', 'Transportation', '2019-09-15 13:00:38', '2019-09-15 13:00:38', null);
INSERT INTO `categories` VALUES ('3', 'Industry', '2019-09-15 13:01:41', '2019-09-15 13:01:41', null);

-- ----------------------------
-- Table structure for data_rows
-- ----------------------------
DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of data_rows
-- ----------------------------
INSERT INTO `data_rows` VALUES ('1', '1', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('2', '1', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('3', '1', 'email', 'text', 'Email', '1', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('4', '1', 'password', 'password', 'Password', '1', '0', '0', '1', '1', '0', '{}', '4');
INSERT INTO `data_rows` VALUES ('5', '1', 'remember_token', 'text', 'Remember Token', '0', '0', '0', '0', '0', '0', '{}', '5');
INSERT INTO `data_rows` VALUES ('6', '1', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '0', '0', '0', '{}', '6');
INSERT INTO `data_rows` VALUES ('7', '1', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '8');
INSERT INTO `data_rows` VALUES ('8', '1', 'avatar', 'image', 'Avatar', '0', '1', '1', '1', '1', '1', '{}', '9');
INSERT INTO `data_rows` VALUES ('9', '1', 'user_belongsto_role_relationship', 'relationship', 'Role', '0', '1', '1', '1', '1', '0', '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', '11');
INSERT INTO `data_rows` VALUES ('10', '1', 'user_belongstomany_role_relationship', 'relationship', 'Roles', '0', '1', '1', '1', '1', '0', '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', '13');
INSERT INTO `data_rows` VALUES ('11', '1', 'settings', 'hidden', 'Settings', '0', '0', '0', '0', '0', '0', '{}', '14');
INSERT INTO `data_rows` VALUES ('12', '2', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('13', '2', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('14', '2', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('15', '2', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('16', '3', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('17', '3', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('18', '3', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('19', '3', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('20', '3', 'display_name', 'text', 'Display Name', '1', '1', '1', '1', '1', '1', null, '5');
INSERT INTO `data_rows` VALUES ('21', '1', 'role_id', 'text', 'Role', '0', '1', '1', '1', '1', '1', '{}', '10');
INSERT INTO `data_rows` VALUES ('22', '1', 'email_verified_at', 'timestamp', 'Email Verified At', '0', '1', '1', '1', '1', '1', '{}', '7');
INSERT INTO `data_rows` VALUES ('23', '1', 'id_number', 'text', 'Id Number', '0', '1', '1', '1', '1', '1', '{}', '12');
INSERT INTO `data_rows` VALUES ('24', '4', 'id', 'number', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('25', '4', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('26', '4', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', '{}', '3');
INSERT INTO `data_rows` VALUES ('27', '4', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '4');
INSERT INTO `data_rows` VALUES ('28', '4', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', '{}', '5');
INSERT INTO `data_rows` VALUES ('29', '5', 'id', 'number', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('30', '5', 'title', 'text', 'Title', '0', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('31', '5', 'img', 'image', 'Img', '0', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('32', '5', 'announcements', 'rich_text_box', 'Announcements', '0', '1', '1', '1', '1', '1', '{}', '4');
INSERT INTO `data_rows` VALUES ('33', '5', 'criterias', 'rich_text_box', 'Criterias', '0', '1', '1', '1', '1', '1', '{}', '5');
INSERT INTO `data_rows` VALUES ('34', '5', 'documents', 'rich_text_box', 'Documents', '0', '1', '1', '1', '1', '1', '{}', '6');
INSERT INTO `data_rows` VALUES ('35', '5', 'covers', 'rich_text_box', 'Covers', '0', '1', '1', '1', '1', '1', '{}', '7');
INSERT INTO `data_rows` VALUES ('36', '5', 'responsibilities', 'rich_text_box', 'Responsibilities', '0', '1', '1', '1', '1', '1', '{}', '8');
INSERT INTO `data_rows` VALUES ('37', '5', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', '{}', '9');
INSERT INTO `data_rows` VALUES ('38', '5', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '10');
INSERT INTO `data_rows` VALUES ('39', '5', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', '{}', '11');
INSERT INTO `data_rows` VALUES ('43', '5', 'program_belongsto_category_relationship', 'relationship', 'categories', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"cat_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', '14');
INSERT INTO `data_rows` VALUES ('44', '5', 'cat_id', 'text', 'Cat Id', '0', '1', '1', '1', '1', '1', '{}', '12');
INSERT INTO `data_rows` VALUES ('45', '6', 'id', 'number', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('46', '6', 'applicant_id', 'text', 'Applicant Id', '0', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('47', '6', 'personal_info', 'text', 'Personal Info', '0', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('48', '6', 'additional_info', 'text', 'Additional Info', '0', '1', '1', '1', '1', '1', '{}', '4');
INSERT INTO `data_rows` VALUES ('49', '6', 'university_education', 'text', 'University Education', '0', '1', '1', '1', '1', '1', '{}', '5');
INSERT INTO `data_rows` VALUES ('50', '6', 'highschool_info', 'text', 'Highschool Info', '0', '1', '1', '1', '1', '1', '{}', '6');
INSERT INTO `data_rows` VALUES ('51', '6', 'lang_prof', 'text', 'Lang Prof', '0', '1', '1', '1', '1', '1', '{}', '7');
INSERT INTO `data_rows` VALUES ('52', '6', 'applicant_desc', 'text', 'Applicant Desc', '0', '1', '1', '1', '1', '1', '{}', '8');
INSERT INTO `data_rows` VALUES ('53', '6', 'documents', 'text', 'Documents', '0', '1', '1', '1', '1', '1', '{}', '9');
INSERT INTO `data_rows` VALUES ('54', '6', 'current_appointment', 'text', 'Current Appointment', '0', '1', '1', '1', '1', '1', '{}', '10');
INSERT INTO `data_rows` VALUES ('55', '6', 'references', 'text', 'References', '0', '1', '1', '1', '1', '1', '{}', '11');
INSERT INTO `data_rows` VALUES ('56', '6', 'scholarships', 'text', 'Scholarships', '0', '1', '1', '1', '1', '1', '{}', '12');
INSERT INTO `data_rows` VALUES ('57', '6', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', '{}', '13');
INSERT INTO `data_rows` VALUES ('58', '6', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '14');
INSERT INTO `data_rows` VALUES ('59', '6', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', '{}', '15');
INSERT INTO `data_rows` VALUES ('60', '6', 'application_belongsto_user_relationship', 'relationship', 'users', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"applicant_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', '16');
INSERT INTO `data_rows` VALUES ('61', '5', 'program_belongsto_form_relationship', 'relationship', 'forms', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Form\",\"table\":\"forms\",\"type\":\"belongsTo\",\"column\":\"form_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', '15');
INSERT INTO `data_rows` VALUES ('62', '5', 'form_id', 'text', 'Form Id', '0', '1', '1', '1', '1', '1', '{}', '13');
INSERT INTO `data_rows` VALUES ('63', '7', 'id', 'number', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('64', '7', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('65', '7', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', '{}', '3');
INSERT INTO `data_rows` VALUES ('66', '7', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '4');
INSERT INTO `data_rows` VALUES ('67', '7', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', '{}', '5');
INSERT INTO `data_rows` VALUES ('68', '8', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('69', '8', 'reviewer_id', 'text', 'Reviewer Id', '0', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('70', '8', 'application_id', 'text', 'Application Id', '0', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('71', '8', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '1', '0', '1', '{}', '4');
INSERT INTO `data_rows` VALUES ('72', '8', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '5');
INSERT INTO `data_rows` VALUES ('73', '8', 'deleted_at', 'timestamp', 'Deleted At', '0', '1', '1', '1', '1', '1', '{}', '6');
INSERT INTO `data_rows` VALUES ('74', '8', 'revs_app_belongsto_application_relationship', 'relationship', 'applications', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Application\",\"table\":\"applications\",\"type\":\"belongsTo\",\"column\":\"application_id\",\"key\":\"id\",\"label\":\"applicant_id\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', '7');
INSERT INTO `data_rows` VALUES ('75', '8', 'revs_app_belongsto_user_relationship', 'relationship', 'users', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"reviewer_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', '8');
INSERT INTO `data_rows` VALUES ('76', '8', 'comment', 'text', 'Comment', '0', '1', '1', '1', '1', '1', '{}', '7');
INSERT INTO `data_rows` VALUES ('77', '8', 'status', 'select_dropdown', 'Status', '0', '1', '1', '1', '1', '1', '{\"default\":\"New\",\"options\":{\"New\":\"New\",\"In progress\":\"In progress\",\"Accepted\":\"Accepted\",\"Declined\":\"Declined\"}}', '8');
INSERT INTO `data_rows` VALUES ('78', '8', 'score', 'text', 'Score', '0', '1', '1', '1', '1', '1', '{}', '9');
INSERT INTO `data_rows` VALUES ('79', '6', 'status', 'select_dropdown', 'Status', '0', '1', '1', '1', '1', '1', '{\"default\":\"New\",\"options\":{\"New\":\"New\",\"In progress\":\"In progress\",\"Accepted\":\"Accepted\",\"Declined\":\"Declined\"}}', '16');
INSERT INTO `data_rows` VALUES ('80', '6', 'application_belongsto_program_relationship', 'relationship', 'programs', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Program\",\"table\":\"programs\",\"type\":\"belongsTo\",\"column\":\"program_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', '17');
INSERT INTO `data_rows` VALUES ('81', '6', 'program_id', 'text', 'Program Id', '0', '1', '1', '1', '1', '1', '{}', '17');

-- ----------------------------
-- Table structure for data_types
-- ----------------------------
DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of data_types
-- ----------------------------
INSERT INTO `data_types` VALUES ('1', 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', null, '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-09-15 12:15:47', '2019-09-15 12:18:42');
INSERT INTO `data_types` VALUES ('2', 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', null, '', '', '1', '0', null, '2019-09-15 12:15:47', '2019-09-15 12:15:47');
INSERT INTO `data_types` VALUES ('3', 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', null, '', '', '1', '0', null, '2019-09-15 12:15:47', '2019-09-15 12:15:47');
INSERT INTO `data_types` VALUES ('4', 'categories', 'categories', 'Category', 'Categories', null, 'App\\Category', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-09-15 12:53:26', '2019-09-15 12:53:26');
INSERT INTO `data_types` VALUES ('5', 'programs', 'programs', 'Program', 'Programs', null, 'App\\Program', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-15 12:58:49', '2019-09-16 13:06:27');
INSERT INTO `data_types` VALUES ('6', 'applications', 'applications', 'Application', 'Applications', null, 'App\\Application', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-16 11:44:47', '2019-09-16 13:25:29');
INSERT INTO `data_types` VALUES ('7', 'forms', 'forms', 'Form', 'Forms', null, 'App\\Form', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-09-16 13:06:45', '2019-09-16 13:06:45');
INSERT INTO `data_types` VALUES ('8', 'revs_apps', 'revs-apps', 'Revs App', 'Revs Apps', null, 'App\\RevsApp', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-09-16 13:08:32', '2019-09-16 13:16:50');

-- ----------------------------
-- Table structure for forms
-- ----------------------------
DROP TABLE IF EXISTS `forms`;
CREATE TABLE `forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of forms
-- ----------------------------
INSERT INTO `forms` VALUES ('1', 'FOUR-YEAR UNDERGRADUATE SCHOLARSHIP', '2019-09-16 13:40:33', '2019-09-16 13:40:33', null);
INSERT INTO `forms` VALUES ('2', 'FEMALE POSTDOCTORAL SCIENTIST SUPPORT AWARDS', '2019-09-16 13:40:56', '2019-09-16 13:40:56', null);
INSERT INTO `forms` VALUES ('3', 'INTERNSHIPS, RESEARCH, AND ENTREPRENEURSHIP SCHOLARSHIPS', '2019-09-16 13:41:16', '2019-09-16 13:41:16', null);
INSERT INTO `forms` VALUES ('4', 'SUPPORT SCHOLARSHIPS', '2019-09-16 13:41:42', '2019-09-16 13:41:42', null);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'admin', '2019-09-15 12:15:49', '2019-09-15 12:15:49');

-- ----------------------------
-- Table structure for menu_items
-- ----------------------------
DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of menu_items
-- ----------------------------
INSERT INTO `menu_items` VALUES ('1', '1', 'Dashboard', '', '_self', 'voyager-boat', null, null, '1', '2019-09-15 12:15:49', '2019-09-15 12:15:49', 'voyager.dashboard', null);
INSERT INTO `menu_items` VALUES ('2', '1', 'Media', '', '_self', 'voyager-images', null, null, '5', '2019-09-15 12:15:49', '2019-09-15 12:15:49', 'voyager.media.index', null);
INSERT INTO `menu_items` VALUES ('3', '1', 'Users', '', '_self', 'voyager-person', null, null, '3', '2019-09-15 12:15:49', '2019-09-15 12:15:49', 'voyager.users.index', null);
INSERT INTO `menu_items` VALUES ('4', '1', 'Roles', '', '_self', 'voyager-lock', null, null, '2', '2019-09-15 12:15:49', '2019-09-15 12:15:49', 'voyager.roles.index', null);
INSERT INTO `menu_items` VALUES ('5', '1', 'Tools', '', '_self', 'voyager-tools', null, null, '9', '2019-09-15 12:15:49', '2019-09-15 12:15:49', null, null);
INSERT INTO `menu_items` VALUES ('6', '1', 'Menu Builder', '', '_self', 'voyager-list', null, '5', '10', '2019-09-15 12:15:49', '2019-09-15 12:15:49', 'voyager.menus.index', null);
INSERT INTO `menu_items` VALUES ('7', '1', 'Database', '', '_self', 'voyager-data', null, '5', '11', '2019-09-15 12:15:49', '2019-09-15 12:15:49', 'voyager.database.index', null);
INSERT INTO `menu_items` VALUES ('8', '1', 'Compass', '', '_self', 'voyager-compass', null, '5', '12', '2019-09-15 12:15:49', '2019-09-15 12:15:49', 'voyager.compass.index', null);
INSERT INTO `menu_items` VALUES ('9', '1', 'BREAD', '', '_self', 'voyager-bread', null, '5', '13', '2019-09-15 12:15:50', '2019-09-15 12:15:50', 'voyager.bread.index', null);
INSERT INTO `menu_items` VALUES ('10', '1', 'Settings', '', '_self', 'voyager-settings', null, null, '14', '2019-09-15 12:15:50', '2019-09-15 12:15:50', 'voyager.settings.index', null);
INSERT INTO `menu_items` VALUES ('11', '1', 'Hooks', '', '_self', 'voyager-hook', null, '5', '13', '2019-09-15 12:15:57', '2019-09-15 12:15:57', 'voyager.hooks', null);
INSERT INTO `menu_items` VALUES ('12', '1', 'Categories', '', '_self', null, null, null, '15', '2019-09-15 12:53:27', '2019-09-15 12:53:27', 'voyager.categories.index', null);
INSERT INTO `menu_items` VALUES ('13', '1', 'Programs', '', '_self', null, null, null, '16', '2019-09-15 12:58:49', '2019-09-15 12:58:49', 'voyager.programs.index', null);
INSERT INTO `menu_items` VALUES ('14', '1', 'Applications', '', '_self', null, null, null, '17', '2019-09-16 11:44:47', '2019-09-16 11:44:47', 'voyager.applications.index', null);
INSERT INTO `menu_items` VALUES ('15', '1', 'Forms', '', '_self', null, null, null, '18', '2019-09-16 13:06:45', '2019-09-16 13:06:45', 'voyager.forms.index', null);
INSERT INTO `menu_items` VALUES ('16', '1', 'Revs Apps', '', '_self', null, null, null, '19', '2019-09-16 13:08:32', '2019-09-16 13:08:32', 'voyager.revs-apps.index', null);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2016_06_01_000001_create_oauth_auth_codes_table', '1');
INSERT INTO `migrations` VALUES ('4', '2016_06_01_000002_create_oauth_access_tokens_table', '1');
INSERT INTO `migrations` VALUES ('5', '2016_06_01_000003_create_oauth_refresh_tokens_table', '1');
INSERT INTO `migrations` VALUES ('6', '2016_06_01_000004_create_oauth_clients_table', '1');
INSERT INTO `migrations` VALUES ('7', '2016_06_01_000005_create_oauth_personal_access_clients_table', '1');
INSERT INTO `migrations` VALUES ('8', '2016_01_01_000000_add_voyager_user_fields', '2');
INSERT INTO `migrations` VALUES ('9', '2016_01_01_000000_create_data_types_table', '2');
INSERT INTO `migrations` VALUES ('10', '2016_05_19_173453_create_menu_table', '2');
INSERT INTO `migrations` VALUES ('11', '2016_10_21_190000_create_roles_table', '2');
INSERT INTO `migrations` VALUES ('12', '2016_10_21_190000_create_settings_table', '2');
INSERT INTO `migrations` VALUES ('13', '2016_11_30_135954_create_permission_table', '2');
INSERT INTO `migrations` VALUES ('14', '2016_11_30_141208_create_permission_role_table', '2');
INSERT INTO `migrations` VALUES ('15', '2016_12_26_201236_data_types__add__server_side', '2');
INSERT INTO `migrations` VALUES ('16', '2017_01_13_000000_add_route_to_menu_items_table', '2');
INSERT INTO `migrations` VALUES ('17', '2017_01_14_005015_create_translations_table', '2');
INSERT INTO `migrations` VALUES ('18', '2017_01_15_000000_make_table_name_nullable_in_permissions_table', '2');
INSERT INTO `migrations` VALUES ('19', '2017_03_06_000000_add_controller_to_data_types_table', '2');
INSERT INTO `migrations` VALUES ('20', '2017_04_21_000000_add_order_to_data_rows_table', '2');
INSERT INTO `migrations` VALUES ('21', '2017_07_05_210000_add_policyname_to_data_types_table', '2');
INSERT INTO `migrations` VALUES ('22', '2017_08_05_000000_add_group_to_settings_table', '2');
INSERT INTO `migrations` VALUES ('23', '2017_11_26_013050_add_user_role_relationship', '2');
INSERT INTO `migrations` VALUES ('24', '2017_11_26_015000_create_user_roles_table', '2');
INSERT INTO `migrations` VALUES ('25', '2018_03_11_000000_add_user_settings', '2');
INSERT INTO `migrations` VALUES ('26', '2018_03_14_000000_add_details_to_data_types_table', '2');
INSERT INTO `migrations` VALUES ('27', '2018_03_16_000000_make_settings_value_nullable', '2');
INSERT INTO `migrations` VALUES ('28', '2018_09_30_110932_create_forms_table', '3');
INSERT INTO `migrations` VALUES ('29', '2018_09_30_142113_create_form_submissions_table', '3');
INSERT INTO `migrations` VALUES ('30', '2018_10_16_000926_add_custom_submit_url_column_to_the_forms_table', '3');

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_access_tokens
-- ----------------------------
INSERT INTO `oauth_access_tokens` VALUES ('2a89a7f5e62da09ee59e4153820483419dfea343585aec9d253ecac1c2f2d149621be2b8f8a38513', '3', '1', 'MyApp', '[]', '0', '2019-09-15 12:38:28', '2019-09-15 12:38:28', '2020-09-15 12:38:28');
INSERT INTO `oauth_access_tokens` VALUES ('446529b168bb454bd76e1f55aea1ff9ed81451ace71b376843c220d43b74569e6e418fdec0614921', '3', '1', 'MyApp', '[]', '0', '2019-09-15 12:42:26', '2019-09-15 12:42:26', '2020-09-15 12:42:26');
INSERT INTO `oauth_access_tokens` VALUES ('56144e620c10c2c9f97dffa0daf3645b7def2543cd005df005d7cc435ff940ab7d3019c9c31b5083', '2', '1', 'MyApp', '[]', '0', '2019-09-15 12:33:07', '2019-09-15 12:33:07', '2020-09-15 12:33:07');
INSERT INTO `oauth_access_tokens` VALUES ('ccb1c85582d90991d299fc8a03058893043c3fb2a5c8172e077e0f2f0b6e601a598ef02f254c8883', '3', '1', 'MyApp', '[]', '0', '2019-09-16 14:04:58', '2019-09-16 14:04:58', '2020-09-16 14:04:58');

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_auth_codes
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_clients
-- ----------------------------
INSERT INTO `oauth_clients` VALUES ('1', null, 'Laravel Personal Access Client', 'LaDm5Qwed8GbD0cYALm6SRlpw7ehob8dacqykmoa', 'http://localhost', '1', '0', '0', '2019-09-15 11:37:15', '2019-09-15 11:37:15');
INSERT INTO `oauth_clients` VALUES ('2', null, 'Laravel Password Grant Client', 'vqVkcz9EhNEbti027LBe0G05bAN29zqr02v67cLt', 'http://localhost', '0', '1', '0', '2019-09-15 11:37:15', '2019-09-15 11:37:15');

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_personal_access_clients
-- ----------------------------
INSERT INTO `oauth_personal_access_clients` VALUES ('1', '1', '2019-09-15 11:37:15', '2019-09-15 11:37:15');

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_refresh_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'browse_admin', null, '2019-09-15 12:15:50', '2019-09-15 12:15:50');
INSERT INTO `permissions` VALUES ('2', 'browse_bread', null, '2019-09-15 12:15:50', '2019-09-15 12:15:50');
INSERT INTO `permissions` VALUES ('3', 'browse_database', null, '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('4', 'browse_media', null, '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('5', 'browse_compass', null, '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('6', 'browse_menus', 'menus', '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('7', 'read_menus', 'menus', '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('8', 'edit_menus', 'menus', '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('9', 'add_menus', 'menus', '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('10', 'delete_menus', 'menus', '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('11', 'browse_roles', 'roles', '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('12', 'read_roles', 'roles', '2019-09-15 12:15:51', '2019-09-15 12:15:51');
INSERT INTO `permissions` VALUES ('13', 'edit_roles', 'roles', '2019-09-15 12:15:52', '2019-09-15 12:15:52');
INSERT INTO `permissions` VALUES ('14', 'add_roles', 'roles', '2019-09-15 12:15:52', '2019-09-15 12:15:52');
INSERT INTO `permissions` VALUES ('15', 'delete_roles', 'roles', '2019-09-15 12:15:52', '2019-09-15 12:15:52');
INSERT INTO `permissions` VALUES ('16', 'browse_users', 'users', '2019-09-15 12:15:52', '2019-09-15 12:15:52');
INSERT INTO `permissions` VALUES ('17', 'read_users', 'users', '2019-09-15 12:15:52', '2019-09-15 12:15:52');
INSERT INTO `permissions` VALUES ('18', 'edit_users', 'users', '2019-09-15 12:15:52', '2019-09-15 12:15:52');
INSERT INTO `permissions` VALUES ('19', 'add_users', 'users', '2019-09-15 12:15:52', '2019-09-15 12:15:52');
INSERT INTO `permissions` VALUES ('20', 'delete_users', 'users', '2019-09-15 12:15:52', '2019-09-15 12:15:52');
INSERT INTO `permissions` VALUES ('21', 'browse_settings', 'settings', '2019-09-15 12:15:53', '2019-09-15 12:15:53');
INSERT INTO `permissions` VALUES ('22', 'read_settings', 'settings', '2019-09-15 12:15:53', '2019-09-15 12:15:53');
INSERT INTO `permissions` VALUES ('23', 'edit_settings', 'settings', '2019-09-15 12:15:53', '2019-09-15 12:15:53');
INSERT INTO `permissions` VALUES ('24', 'add_settings', 'settings', '2019-09-15 12:15:53', '2019-09-15 12:15:53');
INSERT INTO `permissions` VALUES ('25', 'delete_settings', 'settings', '2019-09-15 12:15:53', '2019-09-15 12:15:53');
INSERT INTO `permissions` VALUES ('26', 'browse_hooks', null, '2019-09-15 12:15:57', '2019-09-15 12:15:57');
INSERT INTO `permissions` VALUES ('27', 'browse_categories', 'categories', '2019-09-15 12:53:26', '2019-09-15 12:53:26');
INSERT INTO `permissions` VALUES ('28', 'read_categories', 'categories', '2019-09-15 12:53:26', '2019-09-15 12:53:26');
INSERT INTO `permissions` VALUES ('29', 'edit_categories', 'categories', '2019-09-15 12:53:26', '2019-09-15 12:53:26');
INSERT INTO `permissions` VALUES ('30', 'add_categories', 'categories', '2019-09-15 12:53:26', '2019-09-15 12:53:26');
INSERT INTO `permissions` VALUES ('31', 'delete_categories', 'categories', '2019-09-15 12:53:26', '2019-09-15 12:53:26');
INSERT INTO `permissions` VALUES ('32', 'browse_programs', 'programs', '2019-09-15 12:58:49', '2019-09-15 12:58:49');
INSERT INTO `permissions` VALUES ('33', 'read_programs', 'programs', '2019-09-15 12:58:49', '2019-09-15 12:58:49');
INSERT INTO `permissions` VALUES ('34', 'edit_programs', 'programs', '2019-09-15 12:58:49', '2019-09-15 12:58:49');
INSERT INTO `permissions` VALUES ('35', 'add_programs', 'programs', '2019-09-15 12:58:49', '2019-09-15 12:58:49');
INSERT INTO `permissions` VALUES ('36', 'delete_programs', 'programs', '2019-09-15 12:58:49', '2019-09-15 12:58:49');
INSERT INTO `permissions` VALUES ('37', 'browse_applications', 'applications', '2019-09-16 11:44:47', '2019-09-16 11:44:47');
INSERT INTO `permissions` VALUES ('38', 'read_applications', 'applications', '2019-09-16 11:44:47', '2019-09-16 11:44:47');
INSERT INTO `permissions` VALUES ('39', 'edit_applications', 'applications', '2019-09-16 11:44:47', '2019-09-16 11:44:47');
INSERT INTO `permissions` VALUES ('40', 'add_applications', 'applications', '2019-09-16 11:44:47', '2019-09-16 11:44:47');
INSERT INTO `permissions` VALUES ('41', 'delete_applications', 'applications', '2019-09-16 11:44:47', '2019-09-16 11:44:47');
INSERT INTO `permissions` VALUES ('42', 'browse_forms', 'forms', '2019-09-16 13:06:45', '2019-09-16 13:06:45');
INSERT INTO `permissions` VALUES ('43', 'read_forms', 'forms', '2019-09-16 13:06:45', '2019-09-16 13:06:45');
INSERT INTO `permissions` VALUES ('44', 'edit_forms', 'forms', '2019-09-16 13:06:45', '2019-09-16 13:06:45');
INSERT INTO `permissions` VALUES ('45', 'add_forms', 'forms', '2019-09-16 13:06:45', '2019-09-16 13:06:45');
INSERT INTO `permissions` VALUES ('46', 'delete_forms', 'forms', '2019-09-16 13:06:45', '2019-09-16 13:06:45');
INSERT INTO `permissions` VALUES ('47', 'browse_revs_apps', 'revs_apps', '2019-09-16 13:08:32', '2019-09-16 13:08:32');
INSERT INTO `permissions` VALUES ('48', 'read_revs_apps', 'revs_apps', '2019-09-16 13:08:32', '2019-09-16 13:08:32');
INSERT INTO `permissions` VALUES ('49', 'edit_revs_apps', 'revs_apps', '2019-09-16 13:08:32', '2019-09-16 13:08:32');
INSERT INTO `permissions` VALUES ('50', 'add_revs_apps', 'revs_apps', '2019-09-16 13:08:32', '2019-09-16 13:08:32');
INSERT INTO `permissions` VALUES ('51', 'delete_revs_apps', 'revs_apps', '2019-09-16 13:08:32', '2019-09-16 13:08:32');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES ('1', '1');
INSERT INTO `permission_role` VALUES ('2', '1');
INSERT INTO `permission_role` VALUES ('3', '1');
INSERT INTO `permission_role` VALUES ('4', '1');
INSERT INTO `permission_role` VALUES ('5', '1');
INSERT INTO `permission_role` VALUES ('6', '1');
INSERT INTO `permission_role` VALUES ('7', '1');
INSERT INTO `permission_role` VALUES ('8', '1');
INSERT INTO `permission_role` VALUES ('9', '1');
INSERT INTO `permission_role` VALUES ('10', '1');
INSERT INTO `permission_role` VALUES ('11', '1');
INSERT INTO `permission_role` VALUES ('12', '1');
INSERT INTO `permission_role` VALUES ('13', '1');
INSERT INTO `permission_role` VALUES ('14', '1');
INSERT INTO `permission_role` VALUES ('15', '1');
INSERT INTO `permission_role` VALUES ('16', '1');
INSERT INTO `permission_role` VALUES ('17', '1');
INSERT INTO `permission_role` VALUES ('18', '1');
INSERT INTO `permission_role` VALUES ('19', '1');
INSERT INTO `permission_role` VALUES ('20', '1');
INSERT INTO `permission_role` VALUES ('21', '1');
INSERT INTO `permission_role` VALUES ('22', '1');
INSERT INTO `permission_role` VALUES ('23', '1');
INSERT INTO `permission_role` VALUES ('24', '1');
INSERT INTO `permission_role` VALUES ('25', '1');
INSERT INTO `permission_role` VALUES ('27', '1');
INSERT INTO `permission_role` VALUES ('28', '1');
INSERT INTO `permission_role` VALUES ('29', '1');
INSERT INTO `permission_role` VALUES ('30', '1');
INSERT INTO `permission_role` VALUES ('31', '1');
INSERT INTO `permission_role` VALUES ('32', '1');
INSERT INTO `permission_role` VALUES ('33', '1');
INSERT INTO `permission_role` VALUES ('34', '1');
INSERT INTO `permission_role` VALUES ('35', '1');
INSERT INTO `permission_role` VALUES ('36', '1');
INSERT INTO `permission_role` VALUES ('37', '1');
INSERT INTO `permission_role` VALUES ('38', '1');
INSERT INTO `permission_role` VALUES ('39', '1');
INSERT INTO `permission_role` VALUES ('40', '1');
INSERT INTO `permission_role` VALUES ('41', '1');
INSERT INTO `permission_role` VALUES ('42', '1');
INSERT INTO `permission_role` VALUES ('43', '1');
INSERT INTO `permission_role` VALUES ('44', '1');
INSERT INTO `permission_role` VALUES ('45', '1');
INSERT INTO `permission_role` VALUES ('46', '1');
INSERT INTO `permission_role` VALUES ('47', '1');
INSERT INTO `permission_role` VALUES ('48', '1');
INSERT INTO `permission_role` VALUES ('49', '1');
INSERT INTO `permission_role` VALUES ('50', '1');
INSERT INTO `permission_role` VALUES ('51', '1');

-- ----------------------------
-- Table structure for programs
-- ----------------------------
DROP TABLE IF EXISTS `programs`;
CREATE TABLE `programs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `announcements` longtext COLLATE utf8mb4_unicode_ci,
  `criterias` longtext COLLATE utf8mb4_unicode_ci,
  `documents` longtext COLLATE utf8mb4_unicode_ci,
  `covers` longtext COLLATE utf8mb4_unicode_ci,
  `responsibilities` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cat_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of programs
-- ----------------------------
INSERT INTO `programs` VALUES ('1', 'FOUR-YEAR UNDERGRADUATE SCHOLARSHIP', 'programs\\September2019\\sFoDu6oTuX3biOG45Did.jpg', '<p>The Center of Excellence for Agriculture (COEA) is a USAID-funded project</p>\r\n<p>working toward advancing Egyptian agriculture by developing workforce-</p>\r\n<p>ready graduates and research products that enhance agricultural productivity</p>\r\n<p>and livelihoods and, in doing so, grow to be a highly valued, sustainable</p>\r\n<p>engine for Egyptian agriculture development. The Center of Excellence</p>\r\n<p>for Agriculture COEA is accepting applications for fifty &ldquo;four-year undergraduate</p>\r\n<p>scholarships&rdquo; for the academic year 2019-2020. This is for students graduating</p>\r\n<p>from public high school to pursue undergraduate BSc. degrees (In</p>\r\n<p>English programs) at Faculties of Agriculture in Cairo University, Benha University,</p>\r\n<p>Suez Canal University, and Ain Shams University. This scholarship is</p>\r\n<p>intended for students who are meritorious but cannot enroll for the BSc</p>\r\n<p>Degree in Agriculture (English program) due to their financial conditions.</p>\r\n<p>Recipients will be chosen primarily on financial need and secondarily on</p>\r\n<p>merit. Students interested in pursuing agriculture careers in Egypt and participating</p>\r\n<p>in various COEA activities during their four-years as undergraduate</p>\r\n<p>students are encouraged to apply.</p>', '<ul>\r\n<li>Must be a first level (first year) student beginning undergraduate studies in September 2019</li>\r\n<li>Must be Egyptian national</li>\r\n<li>Must be enrolled in a BS degree-granting undergraduate program in a Faculty of Agriculture at Cairo University or Benha University or Suez Canal</li>\r\n<li>Univrsity or Ain Shams University</li>\r\n<li>Demonstrate good command of English and should have &hellip; score in national TOEFL.</li>\r\n<li>Must demonstrate financial need as a challenge to successfully complete undergraduate studies.</li>\r\n<li>Applicants must be from public high schools.</li>\r\n</ul>', '<ul>\r\n<li>Academic transcript of secondary education.</li>\r\n<li>Proof of financial need: Letter from a Social Solidarity Oce showing student family financial status and income.</li>\r\n<li>Residential electricity bills of the past 12 months (both winter and summer seasons).</li>\r\n<li>Family registration document showing number of family members from Ministry of Interior or its branch oces.</li>\r\n<li>English Language Test Score from the Faculty of Literature at Cairo University or similar/equivalent agencies.</li>\r\n<li>Confirmation that you will participate in various COEA extra-curricular experiences, which may include internships, research projects, entrepreneurship programs job fairs, etc.</li>\r\n</ul>\r\n<p>&nbsp;</p>', '<ul>\r\n<li>Tuition fee &ndash; directly paid to the College/University</li>\r\n<li>Dormitories expenses for lodging will be directly paid to the College/University, in case dorm room is not available, equivalent amount will be given to the awardees</li>\r\n<li>A stipend for meals and school supplies will be directly deposited to his/her bank account</li>\r\n<li>Providing a Laptop/ computer</li>\r\n<li>English language courses</li>\r\n<li>Support for senior research/practicum project</li>\r\n<li>Support for participation in:</li>\r\n</ul>\r\n<ol>\r\n<li>Internships</li>\r\n<li>Research projects</li>\r\n<li>Entrepreneurship programs</li>\r\n<li>Other professional development programs</li>\r\n</ol>', '<ul>\r\n<li>Maintain good academic standing and progress as specified in the university student will be pursuing her/his studies. Recipients must complete their degree in four years.</li>\r\n</ul>\r\n<ul>\r\n<li>Participate in COEA-activities aimed at building skills needed in Egyptian agriculture.</li>\r\n<li>Submit report on annual academic progress (CGPA, project work, etc.).</li>\r\n<li>Submit report on participation in extracurricular COEA experiences.</li>\r\n<li>Work with university faculty and COEA representatives to develop and conduct a senior research/practicum program focusing on a critical issue in Egyptian agriculture.</li>\r\n</ul>', '2019-09-15 13:09:53', '2019-09-16 13:42:06', null, '1', '1');
INSERT INTO `programs` VALUES ('2', 'FOUR-YEAR UNDERGRADUATE SCHOLARSHIP', 'programs\\September2019\\sFoDu6oTuX3biOG45Did.jpg', '<p>The Center of Excellence for Agriculture (COEA) is a USAID-funded project</p>\r\n<p>working toward advancing Egyptian agriculture by developing workforce-</p>\r\n<p>ready graduates and research products that enhance agricultural productivity</p>\r\n<p>and livelihoods and, in doing so, grow to be a highly valued, sustainable</p>\r\n<p>engine for Egyptian agriculture development. The Center of Excellence</p>\r\n<p>for Agriculture COEA is accepting applications for fifty &ldquo;four-year undergraduate</p>\r\n<p>scholarships&rdquo; for the academic year 2019-2020. This is for students graduating</p>\r\n<p>from public high school to pursue undergraduate BSc. degrees (In</p>\r\n<p>English programs) at Faculties of Agriculture in Cairo University, Benha University,</p>\r\n<p>Suez Canal University, and Ain Shams University. This scholarship is</p>\r\n<p>intended for students who are meritorious but cannot enroll for the BSc</p>\r\n<p>Degree in Agriculture (English program) due to their financial conditions.</p>\r\n<p>Recipients will be chosen primarily on financial need and secondarily on</p>\r\n<p>merit. Students interested in pursuing agriculture careers in Egypt and participating</p>\r\n<p>in various COEA activities during their four-years as undergraduate</p>\r\n<p>students are encouraged to apply.</p>', '<ul>\r\n<li>Must be a first level (first year) student beginning undergraduate studies in September 2019</li>\r\n<li>Must be Egyptian national</li>\r\n<li>Must be enrolled in a BS degree-granting undergraduate program in a Faculty of Agriculture at Cairo University or Benha University or Suez Canal</li>\r\n<li>Univrsity or Ain Shams University</li>\r\n<li>Demonstrate good command of English and should have &hellip; score in national TOEFL.</li>\r\n<li>Must demonstrate financial need as a challenge to successfully complete undergraduate studies.</li>\r\n<li>Applicants must be from public high schools.</li>\r\n</ul>', '<ul>\r\n<li>Academic transcript of secondary education.</li>\r\n<li>Proof of financial need: Letter from a Social Solidarity Oce showing student family financial status and income.</li>\r\n<li>Residential electricity bills of the past 12 months (both winter and summer seasons).</li>\r\n<li>Family registration document showing number of family members from Ministry of Interior or its branch oces.</li>\r\n<li>English Language Test Score from the Faculty of Literature at Cairo University or similar/equivalent agencies.</li>\r\n<li>Confirmation that you will participate in various COEA extra-curricular experiences, which may include internships, research projects, entrepreneurship programs job fairs, etc.</li>\r\n</ul>\r\n<p>&nbsp;</p>', '<ul>\r\n<li>Tuition fee &ndash; directly paid to the College/University</li>\r\n<li>Dormitories expenses for lodging will be directly paid to the College/University, in case dorm room is not available, equivalent amount will be given to the awardees</li>\r\n<li>A stipend for meals and school supplies will be directly deposited to his/her bank account</li>\r\n<li>Providing a Laptop/ computer</li>\r\n<li>English language courses</li>\r\n<li>Support for senior research/practicum project</li>\r\n<li>Support for participation in:</li>\r\n</ul>\r\n<ol>\r\n<li>Internships</li>\r\n<li>Research projects</li>\r\n<li>Entrepreneurship programs</li>\r\n<li>Other professional development programs</li>\r\n</ol>', '<ul>\r\n<li>Maintain good academic standing and progress as specified in the university student will be pursuing her/his studies. Recipients must complete their degree in four years.</li>\r\n</ul>\r\n<ul>\r\n<li>Participate in COEA-activities aimed at building skills needed in Egyptian agriculture.</li>\r\n<li>Submit report on annual academic progress (CGPA, project work, etc.).</li>\r\n<li>Submit report on participation in extracurricular COEA experiences.</li>\r\n<li>Work with university faculty and COEA representatives to develop and conduct a senior research/practicum program focusing on a critical issue in Egyptian agriculture.</li>\r\n</ul>', '2019-09-15 13:09:53', '2019-09-16 13:42:16', null, '2', '3');

-- ----------------------------
-- Table structure for revs_apps
-- ----------------------------
DROP TABLE IF EXISTS `revs_apps`;
CREATE TABLE `revs_apps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reviewer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of revs_apps
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', 'Administrator', '2019-09-15 12:15:50', '2019-09-15 12:15:50');
INSERT INTO `roles` VALUES ('2', 'user', 'Normal User', '2019-09-15 12:15:50', '2019-09-15 12:15:50');

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'site.title', 'Site Title', 'Site Title', '', 'text', '1', 'Site');
INSERT INTO `settings` VALUES ('2', 'site.description', 'Site Description', 'Site Description', '', 'text', '2', 'Site');
INSERT INTO `settings` VALUES ('3', 'site.logo', 'Site Logo', '', '', 'image', '3', 'Site');
INSERT INTO `settings` VALUES ('4', 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', '4', 'Site');
INSERT INTO `settings` VALUES ('5', 'admin.bg_image', 'Admin Background Image', '', '', 'image', '5', 'Admin');
INSERT INTO `settings` VALUES ('6', 'admin.title', 'Admin Title', 'Voyager', '', 'text', '1', 'Admin');
INSERT INTO `settings` VALUES ('7', 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', '2', 'Admin');
INSERT INTO `settings` VALUES ('8', 'admin.loader', 'Admin Loader', '', '', 'image', '3', 'Admin');
INSERT INTO `settings` VALUES ('9', 'admin.icon_image', 'Admin Icon Image', '', '', 'image', '4', 'Admin');
INSERT INTO `settings` VALUES ('10', 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', '1', 'Admin');

-- ----------------------------
-- Table structure for translations
-- ----------------------------
DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of translations
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'Admin', 'admin@admin.com', 'users/default.png', null, '$2y$10$yJBWCCReVtC2vPyLgnqldOpnOit79xrrmuPAy2ieYOLB1P3idrG1m', 'OydiZcfaCCZ0vwv0vIWBbAk8W9bZvg8keElqoWaHtweF1T3PvKGlHKxrts56', '', '2019-05-08 10:50:14', '2019-05-08 10:50:14', null);
INSERT INTO `users` VALUES ('3', '2', 'mohamed adel', 'mohamd.adel@innovitics.com', 'users/default.png', null, '$2y$10$Wy359BSMNLTHvs1cIm.mTeJu7JipT.VamR5ptPciD13b28fk.1yLW', null, null, '2019-09-15 12:38:28', '2019-09-15 12:38:28', '29310090101496');

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
