<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RevsApp extends Model
{
    protected $fillable = [
        'id','reviewer_id','application_id','comment','status','score'
    ];
    
    
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];
}
