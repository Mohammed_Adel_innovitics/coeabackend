<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class GrantApp extends Model
{
    
    protected $table = 'grantapps';
    protected $fillable = [
        'id','user_id','personal_info','title','prsubcategory','duration','date_submitted','pi_name','pi_university'
        ,'university_position','email','phone','sign_name','sign_id','project_participants','pi_cvs','team_cvs','concept_noted'
    ];
    
    
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'  
    ];
}
