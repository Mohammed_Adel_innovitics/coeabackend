<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ResearchesCategory extends Model
{
    protected $fillable = [
        'id', 'name'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
}
