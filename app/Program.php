<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Program extends Model
{
    protected $fillable = [
        'id', 'title','img','announcements','criterias','documents','covers','responsibilities','cat_id','form_id'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
}
