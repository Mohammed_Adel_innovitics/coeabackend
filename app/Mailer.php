<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;
use Postmark\PostmarkClient;
class Mailer extends Model
{
    //
    public function sendWelcomeEmail($email,$name){
        $data = array('name'=>"$name");
        Mail::send('welc', $data, function($message) use ($email,$name) {
            $message->to($email, $name)->subject
            ('welcome to coea');
            $message->from('IT@coeag.com','coea');
        }); 
    }
    public function SendApplicantMail($email,$name,$program){
        $data = array('name'=>"$name",'program_name'=>"$program");
        Mail::send('applicantConfirm', $data, function($message) use ($email,$name) {
            $message->to($email, $name)->subject
            ('Application submission confirmation');
            $message->from('IT@coeag.com','coea');
        }); 
    }
    public function send($senderMail,$recipientMail,$templateId,$data){
        
        $client = new PostmarkClient(env("POSTMARK_TOKEN",""));
        //dd($client);
        // Make a request
       // dd($templateId);
        $sendResult = $client->sendEmailWithTemplate(
            $senderMail,
            $recipientMail,
            $templateId,
            $data,
            true, //inline css
            NULL, //tag
            true //trackOpens
            );
        
        if($sendResult->message == 'OK')
        {
            return true;
        }
        
    }
}
