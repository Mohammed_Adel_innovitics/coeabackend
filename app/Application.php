<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Application extends Model
{
    protected $fillable = [
        'id','applicant_id','personal_info','additional_info','university_education','highschool_info','lang_prof','applicant_desc','documents'
        ,'current_appointment','references','scholarships','status','program_id'
    ];
    
    
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];
}
