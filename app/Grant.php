<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Grant extends Model
{
    protected $fillable = [
        'id', 'title','img','announcements','criterias','priorities','process','concept_notes'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
}
