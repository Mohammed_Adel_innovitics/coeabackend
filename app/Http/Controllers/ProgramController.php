<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Status;
use App\MediaUrl;
use App\Category;

class ProgramController extends Controller
{
    //
    public function ListPrograms (Request $request){
        $arr = array();
        $objProg = new Program();
        $objCat = new Category();
        $arrPrograms = $objProg->get()->toArray();
        foreach ($arrPrograms as $index=>$prog){
            $arrPrograms[$index]['img'] = MediaUrl::getUrl().$arrPrograms[$index]['img'];
            $arrPrograms[$index]['cat_id'] = $objCat->where('id',$arrPrograms[$index]['cat_id'])->get()->toArray(); 
        }
        $arr['result'] = $arrPrograms;
        return Status::mergeStatus($arr,200);
    }
    
    public function GetProgById (Request $request){
        $arr = array();
        $objProg = new Program();
        $objCat = new Category();
        $Program = $objProg->where('id',$request['program_id'])->get()->toArray();
        $Program[0]['img'] = MediaUrl::getUrl().$Program[0]['img'];
        $Program[0]['cat_id'] = $objCat->where('id',$Program[0]['cat_id'])->get()->toArray();
        
        $Program[0]['criterias'] = explode(PHP_EOL, $Program[0]['criterias']);
        $criterias = $Program[0]['criterias'] ;
        foreach ($criterias as $key=>$value){
            $criterias [$key] = strip_tags($criterias[$key]);
        }
        $Program[0]['criterias'] = $criterias ;
        //             dd($arrPrograms[$index]['criterias'][0]);
        
        $Program[0]['documents'] = explode(PHP_EOL, $Program[0]['documents']);
        $documents = $Program[0]['documents'] ;
        foreach ($documents as $key=>$value){
            $documents [$key] = strip_tags($documents[$key]);
        }
        $Program[0]['documents'] = $documents;
        
        $Program[0]['covers'] = explode(PHP_EOL, $Program[0]['covers']);
        $covers= $Program[0]['covers'] ;
        foreach ($covers as $key=>$value){
            $covers [$key] = strip_tags($covers[$key]);
        }
        $Program[0]['covers'] = $covers;
        
        $Program[0]['responsibilities'] = explode(PHP_EOL, $Program[0]['responsibilities']);
        $responsibilities = $Program[0]['responsibilities'] ;
        foreach ($responsibilities as $key=>$value){
            $responsibilities[$key] = strip_tags($responsibilities[$key]);
        }
        $Program[0]['responsibilities'] = $responsibilities;
        $arr['result'] = $Program;
        return Status::mergeStatus($arr,200);
    }
}
