<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResearchesSubcategory;
use App\Status;

class ResearchSubcategoryController extends Controller
{
    //
    public function ListSubCategoryById(Request $request){
        $arr = array();
        $objsubCategory = new ResearchesSubcategory();
        $arrCategories = $objsubCategory->where('research_category_id',$request['cat_id'])->get()->toArray();
        $arr['result'] = $arrCategories ;
        return Status::mergeStatus($arr,200);
    }
}
