<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Status;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use App\Application;

class UploaderController extends Controller
{   
//     public function UploadDocs(Request $request){
//         $arrDocs = array();
//         $arr = array();
//         $user_id = Auth::user()->id ;
//         $path = $user_id.'/'.$request['prograrm_id'].'/';
//         $fileName = $request['type'].'.'.$request->file('image')->getClientOriginalExtension();
//         $destinationPath_original = storage_path('app/private/images/').$path;
//         $photoSave = $request->file('image')->move($destinationPath_original, $fileName);
//         if($photoSave){
//             $arr = Status::mergeStatus($arr,200);
//         }else{
//             $arr = Status::mergeStatus($arr,4012);
//         }
//         return $arr ;
//     }
    
    //azur upload
    public function UploadDocs(Request $request){
        $arrDocs = array();
        $arr = array();
        $user_id = Auth::user()->id ;
        $image = $request->file('image');
        $path = $user_id.'/'.$request['prograrm_id'].'/'.$request['type'].'/';
        $azure = \Storage::disk('azure');
        $response = $azure->put($path, file_get_contents($image));
        $arr = Status::mergeStatus($arr,200);
        return $arr;
            
        }
        
        public function UploadDocsGrants(Request $request){
            $arrDocs = array();
            $arr = array();
            $user_id = Auth::user()->id ;
            $image = $request->file('image');
            $path = $user_id.'/grants/'.$request['grant_id'].'/'.$request['type'].'/';
            $azure = \Storage::disk('azure');
            $response = $azure->put($path, file_get_contents($image));
            $arr = Status::mergeStatus($arr,200);
            return $arr;
            
        }
//azur upload
}
