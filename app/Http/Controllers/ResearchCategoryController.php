<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResearchesCategory;
use App\Status;

class ResearchCategoryController extends Controller
{
    public function ListCategories(Request $request){
        $arr = array();
        $objCategory = new ResearchesCategory();
        $arrCategories = $objCategory->get()->toArray();
        $arr['result'] = $arrCategories ; 
        return Status::mergeStatus($arr,200);
    }
    //
}
