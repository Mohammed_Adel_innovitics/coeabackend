<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GrantApp;
use App\Status;
use Illuminate\Support\Facades\Auth;

class GrantAppController extends Controller
{
    //
    public function CheckApplicant(Request $request){ 
        $objApplication = new GrantApp(); 
        $applicant = $objApplication->where('grant_id',$request['grant_id'])->where('user_id',Auth::user()->id)->exists();
        return ($applicant == true) ? Status::printStatus(5003) : Status::printStatus(200);
    }
    public function SubmitApplication(Request $request){
        $objApp = new GrantApp();
        $objApp->user_id = Auth::user()->id ;
        if(isset($request['personal_info']) && $request['personal_info'] != "" && $request['personal_info']!= null){
            $objApp->personal_info = json_encode($request['personal_info']);
        }
        $objApp->grant_id= $request['grant_id'];
        $objApp->title= $request['title'];
        $objApp->prsubcategory= $request['prsubcategory'];
        $objApp->duration = $request['duration'];
        $objApp->date_submitted = $request['date_submitted'];
        $objApp->pi_name= $request['pi_name'];
        $objApp->pi_university= $request['pi_university'];
        $objApp->university_position= $request['university_position'];
        $objApp->email= $request['email'];
        $objApp->phone= $request['phone'];
        $objApp->sign_name= $request['sign_name'];
        $objApp->sign_id= $request['sign_id'];

        $objApp->project_participants= $this->updateAppPhotos($request['grant_id'],'project_participants');
        $objApp->pi_cvs= $this->updateAppPhotos($request['grant_id'],'pi_cvs');
        $objApp->team_cvs= $this->updateAppPhotos($request['grant_id'],'team_cvs');
        $objApp->concept_noted= $this->updateAppPhotos($request['grant_id'],'concept_noted');
        
        
        $objApp->save();
        $id = $objApp->get()->last()->id;
        $arr = array();
        $arr['result'] = $id;
//         $objMailer = new Mailer();
//         $objProg = new Program();
        //         $sendWelcEmail = $objMailer->send('coea.egypt@coeag.org',Auth::user()->email,13876988,['name'=>Auth::user()->name,'program_name'=>$objProg->where('id',$request['program_id'])->get(['title'])[0]['title']]);
        //laravel mailer//
//         $sendWelcEmail = $objMailer->SendApplicantMail(Auth::user()->email,Auth::user()->name,$objProg->where('id',$request['program_id'])->get(['title'])[0]['title']);
        //laravel mailer//
        return Status::mergeStatus($arr,200);
    }
    public function updateAppPhotos($grantId,$type){
        $user_id =  Auth::user()->id ;
        $appPhotosPath = storage_path('app/private/images/').$user_id.'/grants/'.$grantId.'/'.$type.'/';
        $Images = glob($appPhotosPath."*");
        //         dd($Images);
        if(count($Images) > 0){
            foreach ($Images as $index=>$image){
                $Images[$index] = explode('images/',$Images[$index]);
                $Images[$index]= $Images[$index][1];
                $img2 [] = $Images[$index] ;
            }
            $photos = json_encode($img2);
            return $photos ;
        }
    }
}
