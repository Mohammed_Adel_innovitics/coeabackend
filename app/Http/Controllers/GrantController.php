<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grant;
use App\Status;

class GrantController extends Controller
{
    //
    public function ListGrants (Request $request){
        $arr = array();
        $objGrant = new Grant();
        $arrGrants = $objGrant->get()->toArray();
        $arr['result'] = $arrGrants;
        return Status::mergeStatus($arr,200);
    }
    
    public function GetGrantById (Request $request){
        $arr = array();
        $objGrant = new Grant();
//         $objCat = new Category();
        $Grant = $objGrant->where('id',$request['grant_id'])->get()->toArray();
        $Grant[0]['title'] = $Grant[0]['title'].'(Phase 1: COMPETITIVE CONCEPT NOTES)';
        $Grant[0]['criterias'] = explode(PHP_EOL, $Grant[0]['criterias']);
        $criterias = $Grant[0]['criterias'] ;
        foreach ($criterias as $key=>$value){
            $criterias [$key] = strip_tags($criterias[$key]);
        }
        $Grant[0]['criterias'] = $criterias ;
        //             dd($arrPrograms[$index]['criterias'][0]);
        
        $Grant[0]['priorities'] = explode(PHP_EOL, $Grant[0]['priorities']);
        $priorities = $Grant[0]['priorities'] ;
        foreach ($priorities as $key=>$value){
            $priorities[$key] = strip_tags($priorities[$key]);
        }
        $Grant[0]['priorities'] = $priorities;
        
        $Grant[0]['process'] = explode(PHP_EOL, $Grant[0]['process']);
        $process = $Grant[0]['process'] ;
        foreach ($process as $key=>$value){
            $process[$key] = strip_tags($process[$key]);
        }
        $Grant[0]['process'] = $process;
        
        $Grant[0]['concept_notes'] = explode(PHP_EOL, $Grant[0]['concept_notes']);
        $concept_notes = $Grant[0]['concept_notes'] ;
        foreach ($concept_notes as $key=>$value){
            $concept_notes[$key] = strip_tags($concept_notes[$key]);
        }
        $Grant[0]['concept_notes'] = $concept_notes;
        $arr['result'] = $Grant;
        return Status::mergeStatus($arr,200);
    }
}
