<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use Illuminate\Support\Facades\Auth;
use App\Status;
use App\Mailer;
use App\Program;

class ApplicationController extends Controller
{
    //
    public function SubmitApplication(Request $request){
        $objApp = new Application();
        $objApp->applicant_id = Auth::user()->id ; 
        if(isset($request['personal_info']) && $request['personal_info'] != "" && $request['personal_info']!= null){
            $objApp->personal_info = json_encode($request['personal_info']);
        }
        if(isset($request['additional_info']) && $request['additional_info'] != "" && $request['additional_info']!= null){
            $objApp->additional_info= json_encode($request['additional_info']);
        }
        if(isset($request['university_education']) && $request['university_education'] != "" && $request['university_education']!= null){
            $objApp->university_education= json_encode($request['university_education']);
        }
        if(isset($request['highschool_info']) && $request['highschool_info'] != "" && $request['highschool_info']!= null){
            $objApp->highschool_info= json_encode($request['highschool_info']);
        }
        if(isset($request['lang_prof']) && $request['lang_prof'] != "" && $request['lang_prof']!= null){
            $objApp->lang_prof= json_encode($request['lang_prof']);
        }
        $objApp->applicant_desc= $request['applicant_desc'];
        $objApp->documents= $this->updateAppPhotos($request);
        if(isset($request['current_appointment']) && $request['current_appointment'] != "" && $request['current_appointment']!= null){
            $objApp->current_appointment= json_encode($request['current_appointment']);
        }
        if(isset($request['references']) && $request['references'] != "" && $request['references']!= null){
            $objApp->references= json_encode($request['references']);
        }
        if(isset($request['scholarships']) && $request['scholarships'] != "" && $request['scholarships']!= null){
            $objApp->scholarships= json_encode($request['scholarships']);
        }
        $objApp->status = 'New' ; 
        $objApp->program_id= $request['program_id']; 
        $objApp->save();
        $id = $objApp->get()->last()->id;
        $arr = array();
        $arr['result'] = $id;
        $objMailer = new Mailer();
        $objProg = new Program();
//         $sendWelcEmail = $objMailer->send('coea.egypt@coeag.org',Auth::user()->email,13876988,['name'=>Auth::user()->name,'program_name'=>$objProg->where('id',$request['program_id'])->get(['title'])[0]['title']]);
        //laravel mailer//
        $sendWelcEmail = $objMailer->SendApplicantMail(Auth::user()->email,Auth::user()->name,$objProg->where('id',$request['program_id'])->get(['title'])[0]['title']);
        //laravel mailer//
        return Status::mergeStatus($arr,200);
    }
    
    public function GetApplication(Request $request){
        $objApp = new Application();
        $app = $objApp->first()->toArray();
        $app['personal_info'] = json_decode($app['personal_info']);
        $app['additional_info'] = json_decode($app['additional_info']);
        $app['university_education'] = json_decode($app['university_education']);
        $app['highschool_info'] = json_decode($app['highschool_info']);
        $app['lang_prof'] = json_decode($app['lang_prof']);
        $app['current_appointment'] = json_decode($app['current_appointment']);
        $app['references'] = json_decode($app['references']);
        $app['scholarships'] = json_decode($app['scholarships']);
        $arr = array();
        foreach ($app['personal_info'] as $index => $value){
            $arr[explode(':',$value)[0]] = explode(':', $value)[1];
        }
        $app['personal_info'] = $arr ; 
        
        $arr = array();
        foreach ($app['additional_info'] as $index => $value){
            $arr[explode(':',$value)[0]] = explode(':', $value)[1];
        }
        $app['additional_info'] = $arr ; 
        
        $arr = array();
//         dd($app['university_education']);
        foreach ($app['university_education'] as $index => $value){
            $arr[explode(':',$value)[0]] = explode(':', $value)[1];
        }
        $app['university_education'] = $arr ; 
        
        $arr = array();
        foreach ($app['highschool_info'] as $index => $value){
            $arr[explode(':',$value)[0]] = explode(':', $value)[1];
        }
        $app['highschool_info'] = $arr ; 
        
        $arr = array();
        foreach ($app['lang_prof'] as $index => $value){
            $arr[explode(':',$value)[0]] = explode(':', $value)[1];
        }
        $app['lang_prof'] = $arr ; 
        
        $arr = array();
        foreach ($app['current_appointment'] as $index => $value){
            $arr[explode(':',$value)[0]] = explode(':', $value)[1];
        }
        $app['current_appointment'] = $arr ; 
        
        $arr = array();
        foreach ($app['references'] as $index => $value){
            $arr[explode(':',$value)[0]] = explode(':', $value)[1];
        }
        $app['references'] = $arr ; 
        
        $arr = array();
        foreach ($app['scholarships'] as $index => $value){
            $arr[explode(':',$value)[0]] = explode(':', $value)[1];
        }
        $app['scholarships'] = $arr ; 
        dd($app);
        return $app ;
    }
    public function updateAppPhotos(Request $request){
        $user_id =  Auth::user()->id ;
        $appPhotosPath = storage_path('app/private/images/').$user_id.'/'.$request['program_id'].'/';
        $Images = glob($appPhotosPath."*");
//         dd($Images);
        if(count($Images) > 0){
            foreach ($Images as $index=>$image){
                $Images[$index] = explode('images/',$Images[$index]);
                $Images[$index]= $Images[$index][1];
                $img2 [] = $Images[$index] ;
            }
            $photos = json_encode($img2);
            return $photos ;
        }
    }
    public function CheckApplicant(Request $request){
        $objApplication = new Application();
        $applicant = $objApplication->where('program_id',$request['program_id'])->where('applicant_id',Auth::user()->id)->exists();
        return ($applicant == true) ? Status::printStatus(5003) : Status::printStatus(200);
    }
}
