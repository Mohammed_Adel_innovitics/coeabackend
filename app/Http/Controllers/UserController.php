<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Mailer;

class UserController extends Controller
{
    //
    public function Register(Request $request){
        $arr = array();
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:30',
            'email'=>'required|email|unique:users|max:100',
            'password'=> 'required|min:6|max:50',
//             'id_number' =>'required|unique:users|max:14|min:14'
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            
            if($error->first('name')){
                $arr = Status::mergeStatus($arr,4021,'en');
            }
            if($error->first('password')){
                $arr = Status::mergeStatus($arr,4019,'en');
            }
//             if($error->first('id_number')){
//                 $arr = Status::mergeStatus($arr,4020,'en');
//             }
            if($error->first('email')){
                $arr = Status::mergeStatus($arr,4016,'en');
            }
            return $arr;
            
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['user'] = $user;
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $arr['results'] = $success;
        $arr = Status::mergeStatus($arr,200);
        $objMailer = new Mailer();
//         $sendWelcEmail = $objMailer->send('coea.egypt@coeag.org',$user->email,13876987,['name'=>$user->name]);
        //laravel  mailer//
        $sendWelcEmail = $objMailer->sendWelcomeEmail($user->email,$user->name);
        //laravel  mailer//
        
        return $arr;
    }
    public function Login(Request $request){
        $arr = array();
        $validator = Validator::make($request->all(), [
            'email'=>'required|email|max:100',
            'password'=> 'required|min:6|max:50',
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            if($error->first('password')){
                $arr = Status::mergeStatus($arr,4019,'en');
            }
            if($error->first('email')){
                $arr = Status::mergeStatus($arr,5011,'en');
            }
            return $arr;
            
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['user'] = $user;
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $arr['results'] = $success;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }else{
            $arr = Status::mergeStatus($arr,401);
            return $arr;
        }
        
    }
}
