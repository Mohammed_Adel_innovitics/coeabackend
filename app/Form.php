<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Form extends Model
{
    protected $fillable = [
        'id','name'
    ];
    
    
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];
}
