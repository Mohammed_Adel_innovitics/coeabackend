<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaUrl extends Model
{
    //
    static function getUrl(){
        
        return 'http://localhost/coeaBackend/storage/app/public/';
        
    }
    
    static function getImage(){
        
        return 'http://localhost/coeaBackend/storage/app/private/';
        
    }
}
