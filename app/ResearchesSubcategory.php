<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ResearchesSubcategory extends Model
{
    protected $fillable = [
        'id', 'name','research_category_id'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
}
