-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: coea-db-prod.mysql.database.azure.com    Database: coea-prod
-- ------------------------------------------------------
-- Server version	5.6.42.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `applications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `personal_info` longtext COLLATE utf8mb4_unicode_ci,
  `additional_info` longtext COLLATE utf8mb4_unicode_ci,
  `university_education` longtext COLLATE utf8mb4_unicode_ci,
  `highschool_info` longtext COLLATE utf8mb4_unicode_ci,
  `lang_prof` longtext COLLATE utf8mb4_unicode_ci,
  `applicant_desc` longtext COLLATE utf8mb4_unicode_ci,
  `documents` longtext COLLATE utf8mb4_unicode_ci,
  `current_appointment` longtext COLLATE utf8mb4_unicode_ci,
  `references` longtext COLLATE utf8mb4_unicode_ci,
  `scholarships` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applications`
--

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
INSERT INTO `applications` VALUES (9,15,'{\"Family Name\":\"Abualainin\",\"First Name\":\"Mohamed\",\"Middle Name\":\"Adel\",\"Date Of Birth\":\"1993-10-09\",\"Place Of Birth\":\"Cairo\"}','{\"House\":\"sdassad\",\"Street Address\":\"sadasdsad\",\"Area\":\"Damietta\"}',NULL,NULL,NULL,NULL,'[\"15\\/7\\/Recommendation letter.png\",\"15\\/7\\/Research proposal.png\"]',NULL,NULL,NULL,'2019-09-22 20:47:22','2019-09-22 20:47:22',NULL,'New','7'),(10,4,'{\"Family Name\":\"Abualainin5\",\"First Name\":\"Mohamed\",\"Middle Name\":\"Adel\",\"Date Of Birth\":\"1993-10-09\"}','{\"House\":\"rehab home\",\"Street Address\":\"23 street\",\"Area\":\"Cairo\"}',NULL,'{\"Month and Year Degree Completed\":\"02-2011\"}',NULL,NULL,'[\"4\\/9\\/Academic transcript of the previous year.png\",\"4\\/9\\/Number of family members from Ministry of Interior.png\",\"4\\/9\\/Recommendation letter.png\",\"4\\/9\\/Residential electricity bills.png\"]',NULL,NULL,NULL,'2019-09-22 21:24:31','2019-09-22 21:24:31',NULL,'New','9'),(11,4,'{\"Family Name\":\"Abualainin\",\"First Name\":\"Mohamed\",\"Middle Name\":\"Adel\",\"Date Of Birth\":\"1993-10-09\"}','{\"House\":\"rehab home\",\"Street Address\":\"23 street soliman basha\",\"Area\":\"Cairo\"}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-23 08:24:51','2019-09-23 08:24:51',NULL,'New','7'),(12,19,'{\"Family Name\":\"Abualainin\",\"First Name\":\"Mohamed\",\"Middle Name\":\"Adel\",\"Date Of Birth\":\"1993-10-09\"}','{\"House\":\"regasdasd\",\"Street Address\":\"adsasdasdas\",\"Area\":\"Alexandria\"}',NULL,'{\"Name of the School\":\"NAGAT\",\"Month and Year Degree Completed\":\"02-2017\"}',NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-23 09:41:54','2019-09-23 09:41:54',NULL,'New','6'),(13,4,'{\"Family Name\":\"Abualainin\",\"First Name\":\"Mohamed\",\"Middle Name\":\"Adel\",\"Date Of Birth\":\"1993-10-09\"}','{\"House\":\"rehab\",\"Street Address\":\"23 street\",\"Area\":\"Cairo\"}',NULL,'{\"Name of the School\":\"NAJAT\",\"Month and Year Degree Completed\":\"02-2012\"}',NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-23 10:26:26','2019-09-23 10:26:26',NULL,'New','6'),(14,20,'{\"Family Name\":\"Abualainin\",\"First Name\":\"Mohamed\",\"Middle Name\":\"Adel\",\"Date Of Birth\":\"1933-09-10\",\"Place Of Birth\":\"CAiro\"}','{\"House\":\"adssadsada\",\"Street Address\":\"hjghjghjgj\",\"Area\":\"Aswan\"}',NULL,'{\"Name of the School\":\"sdasasdasd\",\"Month and Year Degree Completed\":\"08-2017\"}',NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-23 10:58:46','2019-09-23 10:58:46',NULL,'New','6'),(15,13,'{\"Family Name\":\"Gad\",\"First Name\":\"El-hussien\",\"Middle Name\":\"MOSTAFA\",\"Date Of Birth\":\"22222-09-09\",\"Place Of Birth\":\"naser city ,waha\",\"Gender\":\"Male\"}','{\"House\":\"naser city ,waha\",\"Street Address\":\"naser city ,waha\",\"Area\":\"Giza\",\"Postal Code\":\"11528\",\"Phone\":\"1060 908585\"}',NULL,'{\"Name of the School\":\"El-hussien MOSTAFA Gad\",\"Month and Year Degree Completed\":\"naser city ,waha\",\"Overall Percentage\":\"nas\",\"School Address\":\"naser city ,waha\"}',NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-23 13:42:00','2019-09-23 13:42:00',NULL,'New','6'),(16,13,'{\"Family Name\":\"hussein\",\"First Name\":\"gad\",\"Middle Name\":\"gad\",\"Date Of Birth\":\"19932-10-09\"}','{\"House\":\"asdasdasdasd\",\"Street Address\":\"asdasdasdasd\",\"Area\":\"Beni Suef\"}','{\"PHD_Month and Year Degree Completed\":\"2015-01-01\"}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-23 13:47:38','2019-09-23 13:47:38',NULL,'New','7'),(17,13,'{\"Family Name\":\"Gad\",\"First Name\":\"El-hussien\",\"Middle Name\":\"MOSTAFA\",\"Date Of Birth\":\"22222-11-12\"}','{\"House\":\"naser city ,waha\",\"Street Address\":\"naser city ,waha\",\"Area\":\"Asyut\",\"Postal Code\":\"11528\",\"Phone\":\"1060 908585\"}',NULL,'{\"Month and Year Degree Completed\":\"naser city ,waha\"}',NULL,NULL,NULL,NULL,'{\"references3 email\":\"han_gad_89@hotmail.com\",\"references3 Address\":\"naser city ,waha\"}',NULL,'2019-09-23 13:54:33','2019-09-23 13:54:33',NULL,'New','8'),(18,18,'{\"Family Name\":\"Zier\",\"First Name\":\"Mohamed\",\"Middle Name\":\"Gamal\",\"Date Of Birth\":\"1991-03-29\",\"Place Of Birth\":\"Giza\",\"Gender\":\"Male\"}','{\"House\":\"N16\",\"Street Address\":\"Akoya\",\"Area\":\"Cairo\",\"Postal Code\":\"11453\",\"Phone\":\"01221222902\",\"National ID \\/ Passport\":\"25555555555555\"}','{\"University\":\"AAST\",\"College\":\"Computer Eng\",\"degree\":\"v.good\"}','{\"Name of the School\":\"Nozha\",\"Month and Year Degree Completed\":\"09\\/2008\",\"Overall Percentage\":\"80\",\"School Address\":\"Cairo\"}','{\"Reading\":\"4\",\"Writing\":\"4\",\"Listening\":\"4\",\"Speaking\":\"4\"}','Hello There',NULL,NULL,NULL,NULL,'2019-09-23 16:25:18','2019-09-23 16:25:18',NULL,'New','6'),(19,17,'{\"Family Name\":\"mohamed\",\"First Name\":\"n\",\"Middle Name\":\"goher\",\"Date Of Birth\":\"2019-09-25\"}','{\"House\":\"wwww``\",\"Street Address\":\"wwwww\",\"Area\":\"Faiyum\"}','{\"University\":null,\"College\":null,\"degree\":\"excellent\"}','{\"Name of the School\":\"eeewww\",\"Month and Year Degree Completed\":\"eeeeewwwww\"}','{\"Reading\":\"1\",\"Writing\":\"2\",\"Listening\":\"3\",\"Speaking\":\"2\"}',NULL,NULL,NULL,NULL,NULL,'2019-09-25 10:18:23','2019-09-25 10:18:23',NULL,'New','6'),(20,26,'{\"Family Name\":\"Gad\",\"First Name\":\"El-hussien\",\"Middle Name\":\"MOSTAFA\",\"Date Of Birth\":\"111111-11-11\",\"Place Of Birth\":\"naser city ,waha\",\"Gender\":\"Male\"}','{\"House\":\"naser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,waha\",\"Street Address\":\"naser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,waha\",\"Area\":\"Aswan\",\"Postal Code\":\"11528naser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,waha\",\"Phone\":\"1060 908585\",\"National ID \\/ Passport\":\"14252882568932\"}','{\"University\":\"naser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,waha\",\"College\":\"naser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,waha\",\"degree\":\"excellent\"}','{\"Name of the School\":\"El-hussien MOSTAFA Gad\",\"Month and Year Degree Completed\":\"naser city ,wahanaser city ,wahanasnaser city ,wahanaser city ,wahannaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahaaser city ,wahaer city ,waha\",\"Overall Percentage\":\"nas\",\"School Address\":\"naser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,waha\"}','{\"Reading\":\"2\",\"Writing\":\"2\",\"Listening\":\"2\",\"Speaking\":\"3\"}','naser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanaser city ,wahanase',NULL,NULL,NULL,NULL,'2019-09-25 11:29:25','2019-09-25 11:29:25',NULL,'New','6');
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Agriculture','2019-09-15 13:00:22','2019-09-15 13:00:22',NULL),(2,'Transportation','2019-09-15 13:00:38','2019-09-15 13:00:38',NULL),(3,'Industry','2019-09-15 13:01:41','2019-09-15 13:01:41',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms`
--

DROP TABLE IF EXISTS `forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms`
--

LOCK TABLES `forms` WRITE;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;
INSERT INTO `forms` VALUES (1,'FOUR-YEAR UNDERGRADUATE SCHOLARSHIP','2019-09-16 13:40:33','2019-09-16 13:40:33',NULL),(2,'FEMALE POSTDOCTORAL SCIENTIST SUPPORT AWARDS','2019-09-16 13:40:56','2019-09-16 13:40:56',NULL),(3,'INTERNSHIPS, RESEARCH, AND ENTREPRENEURSHIP SCHOLARSHIPS','2019-09-16 13:41:16','2019-09-16 13:41:16',NULL),(4,'SUPPORT SCHOLARSHIPS','2019-09-16 13:41:42','2019-09-16 13:41:42',NULL);
/*!40000 ALTER TABLE `forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs`
--

DROP TABLE IF EXISTS `programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `programs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `announcements` longtext COLLATE utf8mb4_unicode_ci,
  `criterias` longtext COLLATE utf8mb4_unicode_ci,
  `documents` longtext COLLATE utf8mb4_unicode_ci,
  `covers` longtext COLLATE utf8mb4_unicode_ci,
  `responsibilities` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cat_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs`
--

LOCK TABLES `programs` WRITE;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;
INSERT INTO `programs` VALUES (6,'Four-Year Undergraduate Scholarships','','<p><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">The Center of Excellence for Agriculture (COEA) is a USAID-funded project working toward advancing Egyptian agriculture by developing workforce-ready graduates and research products that enhance agricultural productivity and livelihoods and, in doing so, grow to be a highly valued, sustainable engine for Egyptian agriculture development. The Center of Excellence for Agriculture COEA is accepting applications for fifty &ldquo;four-year undergraduate scholarships&rdquo; for the academic year 2019-2020. This is for students graduating from public high school to pursue undergraduate BSc. degrees (In English programs) at Faculties of Agriculture in Cairo University, Benha University, Suez Canal University, and Ain Shams University. This scholarship is intended for students who are meritorious but cannot enroll for the BSc Degree in Agriculture (English program) due to their financial conditions. Recipients will be chosen primarily on financial need and secondarily on merit. Students interested in pursuing agriculture careers in Egypt and participating in various COEA activities during their four-years as undergraduate students are encouraged to apply.&nbsp;</span></p>','<ul>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Must be a first level (first year) student beginning undergraduate studies in September 2019</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Must be Egyptian national</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Must be enrolled in a BS degree-granting undergraduate program in a Faculty of Agriculture at Cairo University or Benha University or Suez Canal University or Ain Shams University</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Demonstrate good command of English and should have &hellip; score in national TOEFL</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Must demonstrate financial need as a challenge to successfully complete undergraduate studies</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\"><strong><span lang=\"EN-US\" style=\"font-size: 12pt; line-height: 107%;\">Applicants must be from public high schools</span></strong></span></li>\r\n</ul>','<ul>\r\n<li><span style=\"color: #000000; font-size: 16px; text-indent: -24px;\">Academic transcript of secondary education.</span></li>\r\n<li><span style=\"color: #000000; font-size: 16px; text-indent: -24px;\">Proof of financial need: Letter from a Social Solidarity Office showing student family financial status and income.</span></li>\r\n<li><span style=\"color: #000000; font-size: 16px; text-indent: -24px;\">Residential electricity bills of the past 12 months (both winter and summer seasons).</span></li>\r\n<li><span style=\"color: #000000; font-size: 16px; text-indent: -24px;\">English Language Test Score from the Faculty of Literature at Cairo University or similar/equivalent agencies.</span></li>\r\n<li><span style=\"color: #000000; font-size: 16px; text-indent: -24px;\">Confirmation that you will participate in various COEA extra-curricular experiences, which may include internships, research projects, entrepreneurship programs job fairs, etc.</span></li>\r\n</ul>','<ul>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Tuition fee &ndash; directly paid to the College/University</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Dormitories expenses for lodging will be directly paid to the College/University, in case dorm room is not available, equivalent amount will be given to the awardees</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">A stipend for meals and school supplies will be directly deposited to his/her bank account</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Providing a Laptop/ computer</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">English language courses</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Support for senior research/practicum project</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Support for participation in:</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Internships</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Research projects</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Entrepreneurship programs</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Other professional development programs </span></li>\r\n</ul>','<ul>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Maintain good academic standing and progress as specified in the university student will be pursuing her/his studies. Recipients must complete their degree in four years.</span></li>\r\n<li><!--[endif]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Participate in COEA-activities aimed at building skills needed in Egyptian agriculture.</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Submit report on annual academic progress (CGPA, project work, etc.).</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Submit report on participation in extracurricular COEA experiences. </span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Work with university faculty and COEA representatives to develop and conduct a senior research/practicum program focusing on a critical issue in Egyptian agriculture.</span></li>\r\n</ul>','2019-09-19 01:57:07','2019-09-23 09:49:28',NULL,'1','1'),(7,'Female Postdoctoral Scientist Support Awards','','<p class=\"MsoNormal\" style=\"line-height: normal;\"><span lang=\"EN-US\" style=\"font-size: 12.0pt;\">The Center of Excellence for Agriculture (COEA) is a USAID-funded project working toward advancing Egyptian agriculture by developing workforce-ready graduates and research products that enhance agricultural productivity and livelihoods and, in doing so, grow to be a highly valued, sustainable engine for Egyptian agriculture development. The COEA offers 50 (fifty) female postdoctoral scientist awards, 10 awards each for five years. These awards are to sponsor research projects for female post-doctoral students to increase opportunities for women to move up in the ranks of professorship. Recipients will be chosen based on research proposal, alignment of their research goals with the research goals of COEA, and justification on how the scholarships will advance the academic career of the applicant and their research and field expertise. For the academic year 2019/2020, 10 (ten) 12-month scholarships are available. Post-doc fellows are eligible to apply for the scholarship each year, regardless of whether they have received the scholarship in the previous year.</span></p>','<ul>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Must be enrolled as postdoc in Faculty of Agriculture in one of the five COEA universities, namely at Cairo University or Benha University or Suez Canal University or and Ain Shams University or Assuit University.</span></li>\r\n<li><span lang=\"EN-US\" style=\"text-indent: -0.25in; color: black; font-size: 12pt;\">Must be Egyptian national.</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Only female applicants can apply .</span></li>\r\n</ul>','<ul>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Recommendation letter from a university faculty member.</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Doctoral transcript.</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Submit a research proposal outlining the proposed plan for research, including research statement, research objectives, conceptual framework, methods, expected results and impact, and implementation timeline (3,000 words).</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">One-page resume showing publications, and professional contributions, among others.</span></li>\r\n</ul>','<ul>\r\n<li><strong><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Scholarship is up to</span></strong><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\"> up to E&pound;80,000 ($5,000 X 16 Egyptian pound) for one year, this may cover:</span></li>\r\n<li>Bench fees</li>\r\n<li>Stationaries, software</li>\r\n<li>Publication fees</li>\r\n<li>Travel allowances to participate in conferences</li>\r\n</ul>','<ul>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Submit report on research progress and scholarly activities involved in.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Publish COEA supported/related research in peer-reviewed journals and acknowledge the COEA support in the publications/reports.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\"><span style=\"font-size: 12pt; text-align: justify; text-indent: -0.25in;\">Participate and provide expert advice in COEA as needed.</span><br /></span></p>\r\n</li>\r\n</ul>','2019-09-19 01:57:07','2019-09-20 14:04:05',NULL,'1','2'),(8,'Internships, Research, and Entrepreneurship Scholarships','','<p class=\"MsoNormal\" style=\"line-height: normal;\"><span lang=\"EN-US\" style=\"font-size: 12.0pt;\">The Center of Excellence for Agriculture (COEA) is a USAID-funded project working toward advancing Egyptian agriculture by developing workforce-ready graduates and research products that enhance agricultural productivity and livelihoods and, in doing so, grow to be a highly valued, sustainable engine for Egyptian agriculture development. The COEA is offering 50 internships, research, and entrepreneurship scholarships, 10 scholarships each for five years, based on need, merit, and alignment of proposed project&rsquo;s goals with goals and research products of the COEA. The scholarship could be for 3-6 months depending on the students&rsquo; needs. The purpose of the internship program is to help student get on-site and hands-on work experience directly related to career goals and this will be done under the supervision of expert of their fields. Research scholarships are to provide opportunities to students to get first-hand experiences of doing systematic research. Entrepreneurship scholarship is intended to those students who are interested to pilot and learn about profitable agribusiness. For the current academic year 2019/2020, ten scholarships are available.</span></p>','<ul>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Must be student pursuing undergraduate degree programs in Faculty of Agriculture in one of the five COEA universities &ndash; Cairo University or Benha University or Suez Canal University or Assuit University, or Ain Shams University.</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Sophomore or higher-level undergraduate student.</span></li>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; color: black; mso-ansi-language: EN-US; mso-fareast-language: EN-GB; mso-bidi-language: AR-SA;\">Demonstrate good command of English.</span></li>\r\n</ul>','<ul>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Recommendation letter from academic advisor indicating your character and experience.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Academic transcript of the previous year.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span lang=\"EN-US\" style=\"text-indent: -0.25in; font-size: 12pt; color: black;\">Letter from English Faculty </span><span lang=\"EN-US\" style=\"text-indent: -0.25in; font-size: 12pt; color: red;\">member</span><span lang=\"EN-US\" style=\"text-indent: -0.25in; font-size: 12pt; color: black;\"> or certificate from English language testing agency indicating English language proficiency.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Letter from a Social Solidarity Office describing family&rsquo;s financial status and income.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span lang=\"EN-US\" style=\"text-indent: -0.25in; font-size: 12pt; color: black;\">Family registration document showing number of family members from Ministry of Interior or its branch offices </span><span dir=\"RTL\" lang=\"AR-SA\" style=\"text-indent: -0.25in; font-size: 12pt; color: black;\">قي) عائلي</span><span lang=\"EN-US\" style=\"text-indent: -0.25in; font-size: 12pt; color: black;\">).</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">A plan or research proposal (maximum 1000 words) describing your motivation and plan for the intended internship/research/entrepreneurship </span><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">project/program and institutions you want to pursue the training.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Confirmation that you are willing to participate in various COEA extra-curricular experiences, which may include internships, research projects, entrepreneurship programs job fairs, etc.</span></p>\r\n</li>\r\n</ul>','<ul>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Living and affiliated cost</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Stationaries</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Support to attain training, for example, research methods, data collection, data analysis and report/article writing, data management, agribusiness management, innovations, English language, soft skills, etc.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Support for travel to attend research seminars or agribusiness fairs or job fairs.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Support for research/practicum project.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Support for participation in entrepreneurship programs.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Support for the software and laboratory reagents required for the project.</span></p>\r\n</li>\r\n</ul>','<ul>\r\n<li><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Submit report on academic progress (CGPA, project work, etc.)</span></li>\r\n<li><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Submit report on participation in extracurricular COEA experiences.</span></li>\r\n<li><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Submit report on internship projects or entrepreneurship completed with the support from this scholarship.</span></li>\r\n</ul>','2019-09-19 01:57:07','2019-09-20 14:12:04',NULL,'1','3'),(9,'Support Scholarships','','<p class=\"MsoNormal\" style=\"line-height: normal;\"><span lang=\"EN-US\" style=\"font-size: 12.0pt;\">The Center of Excellence for Agriculture (COEA) is a USAID-funded project working toward advancing Egyptian agriculture by developing workforce-ready graduates and research products that enhance agricultural productivity and livelihoods and, in doing so, grow to be a highly valued, sustainable engine for Egyptian agriculture development. <span style=\"color: black;\">The COEA offers seventy-five (75), fifteen (15) per year for five years, needs-based support scholarships for students pursuing undergraduate degrees in the Faculties of Agriculture at Benha University, Cairo University, Assiut University, Ain Shams University, and Suez Canal University. In the academic year 2019/2020 the COEA plans to offer 15 scholarships. These one-year scholarships are geared toward financially disadvantaged students. Students are eligible to apply for the scholarship each year regardless of whether they have received the scholarship in the previous year.</span></span></p>','<ul>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Must be Egyptian national.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Must be pursuing a BS degree-granting undergraduate program in a Faculty of Agriculture at Cairo University or Benha University or Suez Canal University or Assuit University, or Ain Shams University.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Must demonstrate financial need as a challenge to successfully complete undergraduate studies.</span></p>\r\n</li>\r\n</ul>','<ul>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Letter of recommendation (LOR) from academic advisor indicating student&rsquo;s character and experience. For freshmen no LOR will be required.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Academic transcript of the previous year, and for the freshmen higher secondary school transcript. </span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Letter from a Social Solidarity Office describing applicant&rsquo;s family&rsquo;s financial status and income. </span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Resident electricity bills of the past 12 months (both summer and winter seasons).</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Family registration document showing number of family members from Ministry of Interior or its branch office </span><span dir=\"RTL\" lang=\"AR-SA\" style=\"font-size: 12.0pt; color: black;\">قي) عائلي</span><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">).</span></p>\r\n</li>\r\n<li>\r\n<h6 class=\"MsoNormal\" style=\"text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-family: Calibri, sans-serif; font-size: 12pt; text-indent: -0.25in;\">Confirmation that student is willing to participate in various COEA extra-curricular experiences, which may include internships, research projects, entrepreneurship programs job fairs, etc.</span></h6>\r\n</li>\r\n</ul>','<p class=\"MsoNormal\"><strong><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%; color: black;\">Scholarship covers </span></strong><span lang=\"EN-US\" style=\"font-size: 12.0pt; line-height: 107%;\">18,00 euro(tuition for one year?); Stipend: 8,000 euro(8 months); Leaving expenses: 6,400 euro (for 8 months), and for training/workshops: 3000 euro (for one workshop) </span><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Living and affiliated cost and allowances</span></p>\r\n<ul>\r\n<li class=\"MsoNormal\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Tuition fees</span></li>\r\n<li class=\"MsoNormal\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Support for senior research/practicum project</span></li>\r\n<li class=\"MsoNormal\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">English language courses </span></li>\r\n<li class=\"MsoNormal\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Support for participation in:</span></li>\r\n</ul>\r\n<ol>\r\n<li><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Internships</span></li>\r\n<li><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Research projects</span></li>\r\n<li><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Entrepreneurship programs</span></li>\r\n<li><span style=\"color: black; font-family: Calibri, sans-serif; font-size: 12pt; text-align: justify; text-indent: -0.25in;\">Other professional development programs&nbsp;</span></li>\r\n</ol>','<ul>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><!-- [if !supportLists]--><span lang=\"EN-US\" style=\"font-size: 12.0pt; color: black;\">Maintain good academic standing and progress as specified in the university student will be pursuing her/his studies.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Participate in COEA-activities aimed at building skills needed in Egyptian agriculture.</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Submit report on annual academic progress (CGPA, project work, etc.).</span></p>\r\n</li>\r\n<li>\r\n<p class=\"MsoNormal\" style=\"text-align: justify; text-indent: -.25in; line-height: normal; mso-list: l0 level1 lfo1; border: none; mso-padding-alt: 31.0pt 31.0pt 31.0pt 31.0pt; mso-border-shadow: yes; margin: 0in 0in .0001pt .5in;\"><span style=\"color: black; font-size: 12pt; text-indent: -0.25in;\">Submit report on participation in extracurricular COEA experiences.</span></p>\r\n</li>\r\n</ul>','2019-09-19 01:57:07','2019-09-20 14:24:11',NULL,'1','4');
/*!40000 ALTER TABLE `programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revs_apps`
--

DROP TABLE IF EXISTS `revs_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `revs_apps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reviewer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revs_apps`
--

LOCK TABLES `revs_apps` WRITE;
/*!40000 ALTER TABLE `revs_apps` DISABLE KEYS */;
/*!40000 ALTER TABLE `revs_apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-13 15:59:39
