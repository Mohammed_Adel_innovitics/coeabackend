<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// header('Access-Control-Allow-Credentials: true');

// header('Access-Control-Allow-Origin: grants.coeag.org');
// header('Access-Control-Allow-Origin: ets.coeag.org');
// header('Access-Control-Allow-Methods: *');
// header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::group(['prefix' => 'user'], function() {
        Route::post('register', 'UserController@Register');
        Route::post('login', 'UserController@Login');      
    });
    
        Route::group(['prefix' => 'programs'], function() {
            Route::get('list', 'ProgramController@ListPrograms');
            Route::post('get', 'ProgramController@GetProgById');
            
        });
        Route::group(['prefix' => 'application','middleware' => 'auth:api'], function() {
            Route::post('apply', 'ApplicationController@SubmitApplication');
            Route::post('get', 'ApplicationController@GetApplication');
            Route::post('docs/upload', 'UploaderController@UploadDocs');
            Route::post('check', 'ApplicationController@CheckApplicant');
            
        });
        
            Route::group(['prefix' => 'grants'], function() {
                Route::get('list', 'GrantController@ListGrants');
                Route::post('get', 'GrantController@GetGrantById');
                
            });
            
                Route::group(['prefix' => 'grantapp','middleware' => 'auth:api'], function() {  
                    Route::post('apply', 'GrantAppController@SubmitApplication');
//                     Route::post('get', 'ApplicationController@GetApplication');
                    Route::post('docs/upload', 'UploaderController@UploadDocsGrants');
                    Route::post('check', 'GrantAppController@CheckApplicant');
                    Route::get('categories', 'ResearchCategoryController@ListCategories');
                    Route::post('subcategories', 'ResearchSubcategoryController@ListSubCategoryById');
                    
                });
            